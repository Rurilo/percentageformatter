import java.util.function.DoubleFunction;

public class PercentageFormatter {
    public static final DoubleFunction<String> INSTANCE = value -> String.format("%s%.1f %%", value < 0 ? "-" : "", Math.round(Math.abs(value) * 1000.0) / 10.0);

    public static void main(String[] args) {
        // Testing the lambda expression
        System.out.println(PercentageFormatter.INSTANCE.apply(0.42));    // Output: 42.0 %
        System.out.println(PercentageFormatter.INSTANCE.apply(0.427));   // Output: 42.7 %
        System.out.println(PercentageFormatter.INSTANCE.apply(0.4273));  // Output: 42.7 %
        System.out.println(PercentageFormatter.INSTANCE.apply(0.4275));  // Output: 42.8 %
        System.out.println(PercentageFormatter.INSTANCE.apply(-0.4275)); // Output: -42.8 %
    }
}